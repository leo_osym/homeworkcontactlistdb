package com.example.homeworkcontactlist

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class Contact(
    @PrimaryKey
    open var Id: Int = 0,
    open var FullName: String = "",
    open var UserEmail: String = "",
    open var ImageId: Int? = null,
    open var City: String? = null,
    open var Details: String? = null): RealmObject() {
}
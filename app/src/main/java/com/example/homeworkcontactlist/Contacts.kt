package com.example.homeworkcontactlist

class Contacts {
    companion object {
        var images = arrayOf(R.drawable.icon_1,
            R.drawable.icon_2, R.drawable.icon_3,
            R.drawable.icon_4, R.drawable.icon_5)

        var All: ArrayList<Contact> = ArrayList()
        init{
            All.add(Contact(0,"John Cena", "john.cena@i.ua", images.random(),
                 "New York", "Champ"))
            All.add(Contact(1,"Axel Primeiro", "axel.primeiro@i.ua", images.random(),
                 "Dnipro", "Someone I don't know"))
            All.add(Contact(2, "Chloe Price", "chloe.price@ex.ua", images.random(),
                 "Arcadia Bay", "Cool chick"))
            All.add(Contact(3,"Maxine Caulfield", "max.the.great@i.ua",  images.random(),
                 "Arcadia Bay", "Student"))
            All.add(Contact(4,"Mark Jefferson", "mark.jefferson@ex.ua", images.random(),
                 "Arcadia Bay", "Photography teacher"))
            All.add(Contact(5,"Dean Moriarty", "dean.moriarty@i.ua",null,
                 "Denver", "Traveller and adventurer"))
        }
    }
}
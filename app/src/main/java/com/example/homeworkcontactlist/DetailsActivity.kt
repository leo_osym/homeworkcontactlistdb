package com.example.homeworkcontactlist

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_details.*
import io.realm.Realm

class DetailsActivity: AppCompatActivity() {

    private var entry_id = -1
    lateinit var contact: Contact
    lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        getActionBar()?.setDisplayHomeAsUpEnabled(true)

        entry_id = intent.extras!!.get("ID") as Int
        contact = DBHelper.getContact(entry_id)!!

        fillData()
    }

    // change home button action
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        var id = item?.getItemId() ?: 0

        if (id == android.R.id.home) {
            val intent = Intent(baseContext, MainActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun fillData(){
        // set content
        userName.text = "${contact.FullName}"
        userImage.setImageResource(contact.ImageId ?: R.drawable.icon_1)
        userEmail.text = contact.UserEmail
        userCity.text = "City: ${contact.City}"
        userDetails.text = contact.Details
    }
}

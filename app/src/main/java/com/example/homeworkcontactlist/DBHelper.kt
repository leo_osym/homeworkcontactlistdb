package com.example.homeworkcontactlist

import io.realm.Realm
import java.security.MessageDigest

object DBHelper: DatabaseHelperInterface {

    private lateinit var realm: Realm
    public var isAuthenticated = false

    fun initDB(){
        realm = Realm.getDefaultInstance()
        initDataIfNotExist()
    }

    override fun addContact(contact: Contact): Boolean {
        try {
            realm.executeTransaction {
                it.copyToRealm(contact)}
            return true
        } catch (e: Exception) {
            print(e)
            return false
        }
    }

    override fun deleteContact(id: Int): Boolean {
        try {
            realm.executeTransaction {
                it.where(Contact::class.java).equalTo("Id", id).findFirst()!!.deleteFromRealm()}
            return true
        } catch (e: Exception) {
            print(e)
            return false
        }
    }

    override fun editContact(contact: Contact): Boolean {
        try {
            realm.executeTransaction {
                it.copyToRealmOrUpdate(contact)}
            return true
        } catch (e: Exception) {
            print(e)
            return false
        }
    }

    override fun getContact(id: Int): Contact? {
        var contact = Contact()
        try {
            realm.executeTransaction {
                contact = it.where(Contact :: class.java).equalTo("Id", id).findFirst()!!
            }
        } catch (ex: Exception) {
            print(ex)
        }
        return contact
    }

    fun getAllContacts(): ArrayList<Contact> {
        var contacts = ArrayList<Contact>()
        try {
            realm.executeTransaction {
                var realmData = realm.where(Contact::class.java).findAll()
                for(contact in realmData){
                    contacts.add(contact)
                }
            }
        } catch (ex: Exception) {
            print(ex)
        }

        return contacts
    }

    // get unique index for new contact
    fun getUniqueContactIndex(): Int {
        var key = 0
        try {
            realm.executeTransaction {
                key = it.where(Contact::class.java).max("Id")!!.toInt() + 1
            }
        } catch(ex: Exception) {
            print(ex)
        }
        return key
    }

    // init database data
    fun initDataIfNotExist(): Boolean {
        try {
            if(realm.isEmpty){
                for(contact in Contacts.All){
                    this.addContact(contact)
                }
                var user = User()
                user.Email = "admin"
                user.Password = hashString("admin")
                addUser(user)
                return true
            }
            return false
        } catch (e: Exception) {
            print(e)
            return false
        }
    }

    // add new user to DB
    fun addUser(user: User): Boolean {
        try {
            user.Id = getUniqueUserIndex()
            realm.executeTransaction {
                it.copyToRealm(user)}
            return true
        } catch (e: Exception) {
            print(e)
            return false
        }
    }

    // get unique index for new user
    fun getUniqueUserIndex(): Int {
        var key = 0
        try {
            realm.executeTransaction {
                key = it.where(User::class.java).max("Id")!!.toInt() + 1
            }
        } catch(ex: Exception) {
            print(ex)
        }
        return key
    }

    // get user from DB
    fun getUser(email: String): User? {
        var user = User()
        try {
            realm.executeTransaction {
                user = it.where(User :: class.java).equalTo("Email", email).findFirst()!!
            }
        } catch (ex: Exception) {
            print(ex)
        }
        return user
    }

    // function to hash passwords
    // honestly stolen from https://www.samclarke.com/kotlin-hash-strings/
    fun hashString(input: String, type: String = "SHA-256"): String {
        val HEX_CHARS = "0123456789ABCDEF"
        val bytes = MessageDigest
            .getInstance(type)
            .digest(input.toByteArray())
        val result = StringBuilder(bytes.size * 2)

        bytes.forEach {
            val i = it.toInt()
            result.append(HEX_CHARS[i shr 4 and 0x0f])
            result.append(HEX_CHARS[i and 0x0f])
        }

        return result.toString()
    }
}
package com.example.homeworkcontactlist

import io.realm.Realm

interface DatabaseHelperInterface {
    fun addContact(contact: Contact): Boolean
    fun deleteContact(id: Int): Boolean
    fun editContact(contact: Contact): Boolean
    fun getContact(id: Int): Contact?
}
package com.example.homeworkcontactlist

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class User(
    @PrimaryKey
    open var Id: Int = 0,
    open var Email: String = "",
    open var Password: String = "",
    open var FirstName: String = "",
    open var LastName: String = "",
    open var Phone: String = ""): RealmObject() {
}

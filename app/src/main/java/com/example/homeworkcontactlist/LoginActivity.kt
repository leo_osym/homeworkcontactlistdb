package com.example.homeworkcontactlist

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    lateinit var password: String
    lateinit var email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        // hide action bar
        var actionBar = getSupportActionBar()
        actionBar!!.hide()
        // init DB
        Realm.init(this)
        DBHelper.initDB()

        // if already authenticated
        if(DBHelper.isAuthenticated == true){
            goToActivity()
        }

        login_button.setOnClickListener {
            // hide keyboard
            var imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)

            email = login_email_field.text.toString()
            password = login_password_field.text.toString()

            authenticateUser()
        }

        login_register_field.setOnClickListener {
            val intent = Intent(baseContext, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    private fun authenticateUser(){
        var user = DBHelper.getUser(email) ?: User()

        if(user.Email == ""){
            Toast.makeText(this, "User with email \"$email\" is not registered yet!", Toast.LENGTH_SHORT).show()

        } else if ((user.Password != DBHelper.hashString(password))){
            Toast.makeText(this, "Password is incorrect! Try again!", Toast.LENGTH_SHORT).show()

        } else if ((user.Password == DBHelper.hashString(password))){
            goToActivity()
        }
        DBHelper.isAuthenticated = true
    }

    private fun goToActivity(){
        if(intent.hasExtra("openWithId")){
            var id = intent.getIntExtra("openWithId",-1)
            if(id != -1){
                val intent = Intent(baseContext, DetailsActivity::class.java)
                intent.putExtra("ID", id)
                startActivity(intent)
            }
        } else {
            val intent = Intent(baseContext, MainActivity::class.java)
            startActivity(intent)
        }
    }
}

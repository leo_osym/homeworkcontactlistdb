package com.example.homeworkcontactlist

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_add_edit.*

class AddEditActivity : AppCompatActivity() {

    private var isEditing = false
    private var entryId = -1
    private lateinit var tempContact: Contact

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit)
        getActionBar()?.setDisplayHomeAsUpEnabled(true)

        if(intent.hasExtra("EDIT")) {
            isEditing = intent.getBooleanExtra("EDIT", false)
            entryId = intent.getIntExtra("ID", -1)
        }

        // if contact is editing - load its data to fields
        fillFields()

        // Floating action button - Save
        val fab: View = findViewById(R.id.fab)
        fab.setOnClickListener {
            // simple input validation
            if(!add_edit_usermail.text.isEmpty() && !add_edit_usermail.text.isEmpty()) {

                saveChanges()

                val intent = Intent(baseContext, MainActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Name and Email fields couldn't be blank!",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private  fun fillFields(){
        if(isEditing == true) {
            tempContact = DBHelper.getContact(entryId)!!
            add_edit_username.setText(tempContact.FullName)
            add_edit_usermail.setText(tempContact.UserEmail)
            add_edit_city.setText(tempContact.City)
            add_edit_details.setText(tempContact.Details)
        }
    }

    private fun getDataFromFields():Contact {
        var images = arrayOf(R.drawable.icon_1,
            R.drawable.icon_2, R.drawable.icon_3,
            R.drawable.icon_4, R.drawable.icon_5)

        var name = add_edit_username.text.toString()
        var mail = add_edit_usermail.text.toString()
        var city = add_edit_city.text.toString()
        var details = add_edit_details.text.toString()

        return Contact(entryId, name, mail, images.random(), city, details)
    }

    private fun saveChanges(){
        var contact = getDataFromFields()
        var notificationId = 0
        var notification = NotificationHelper()
        if (isEditing == true) {
            var res = DBHelper.editContact(contact)
            if(res) {
                var notificationIntent = Intent(this, LoginActivity::class.java)
                notificationIntent.putExtra("openWithId",contact.Id)
                notification.makeNotifications(this, R.drawable.edit_white_24dp,"Contact edited","Contact ${contact.FullName} was successfully updatedf!", notificationIntent)
            }
        } else {
            contact.Id = DBHelper.getUniqueContactIndex()
            var res = DBHelper.addContact(contact)
            if(res) {
                var notificationIntent = Intent(this, LoginActivity::class.java)
                notificationIntent.putExtra("openWithId",contact.Id)
                notification.makeNotifications(this, R.drawable.add_white_24dp,"Contact added","Contact ${contact.FullName} was successfully added!", notificationIntent)
            }
        }
    }
}

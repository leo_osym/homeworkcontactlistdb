package com.example.homeworkcontactlist

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    lateinit var  firstName: String
    lateinit var  lastName: String
    lateinit var  password: String
    lateinit var  repeatedPassword: String
    lateinit var  phone: String
    lateinit var  email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        // hide action bar
        var actionBar = getSupportActionBar()
        actionBar!!.hide()

        register_button.setOnClickListener {
            getData()
            addUserToDB()
        }

        // return to previous activity
        register_return_field.setOnClickListener {
            val intent = Intent(baseContext, LoginActivity::class.java)
            startActivity(intent)
        }

        // tap somewhere on the screen to hide keyboard
        main_layout.setOnClickListener {
            var imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        }

    }

    private fun addUserToDB(){
        // if input is valid
        if(checkIfDataIsValid()){
            // if user with this email is not in db
            if(DBHelper.getUser(email) != null){
                var user = User(0, email, DBHelper.hashString(password), firstName, lastName,phone)
                var result = DBHelper.addUser(user)
                // if user has been written to db successfully
                if(result){
                    val intent = Intent(baseContext, LoginActivity::class.java)
                    startActivity(intent)
                    // if error occurs during writing to db
                } else {
                    Toast.makeText(this, "Can't add this user! Try again!", Toast.LENGTH_SHORT).show()
                }
                // if user is already exists
            } else {
                Toast.makeText(this, "This user is already exist! Try again!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    // get data from fields
    private fun getData(){
        firstName = register_firstname_field.text.toString()
        lastName = register_lastname_field.text.toString()
        password = register_password_field.text.toString()
        repeatedPassword = register_password_repeat_field.text.toString()
        phone = register_phone_field.text.toString()
        email = register_email_field.text.toString()
    }

    // validate fields data
    private fun checkIfDataIsValid():Boolean {
        var firstNameValidationMessage = validateName(firstName)
        var lastNameValidationMessage = validateName(lastName)
        var passwordValidationMessage = validatePassword(password)
        var emailValidationMessage = validateEmail(email)
        var phoneValidationMessage = validatePhone(phone)

        when{
            (emailValidationMessage != "OK") -> {
                Toast.makeText(this, "Email $emailValidationMessage", Toast.LENGTH_SHORT).show()
                return false
            }
            (password != repeatedPassword) -> {
                Toast.makeText(this, "Passwords don't match!", Toast.LENGTH_SHORT).show()
                return false
            }
            (passwordValidationMessage != "OK") -> {
                Toast.makeText(this, "Password $passwordValidationMessage", Toast.LENGTH_SHORT).show()
                return false
            }
            (firstNameValidationMessage != "OK") -> {
                Toast.makeText(this, "FirstName $firstNameValidationMessage", Toast.LENGTH_SHORT).show()
                return false
            }
            (lastNameValidationMessage != "OK") -> {
                Toast.makeText(this, "LastName $lastNameValidationMessage", Toast.LENGTH_SHORT).show()
                return false
            }
            (phoneValidationMessage != "OK") -> {
                Toast.makeText(this, "Phone $phoneValidationMessage", Toast.LENGTH_SHORT).show()
            }
            else -> return true
        }
        return false
    }

    private fun validateName(name: String): String {
        when {
            (name.length == 0 || name == " ") -> return " could't be blank !"
            (name.length < 2 || name.length > 200) -> return " should be between 2 and 200 characters!"
            (Regex(pattern = """[^\w\s'-]""").containsMatchIn(input = name)) -> return  " can only contains letters!"
            (Regex(pattern = """^[\s-']|[\s-']$""").containsMatchIn(input = name)) -> return " can't start or end with not a letter!"
            else -> return "OK"
        }
    }
    private fun validatePassword(pwd: String): String {
        when {
            (pwd.length == 0 || pwd == " ") -> return " could't be blank !"
            (pwd.length < 5 || pwd.length > 32)  -> return " should be between 6 and 32 characters!"
            (Regex(pattern = """[^\w\d]""").containsMatchIn(input = pwd)) -> return " couldn't consists of other characters except numbers and letters!"
            else -> return "OK"
        }
    }
    private fun validateEmail(email: String): String{
        when {
            (email.length == 0 || email == " ") -> return " could't be blank !"
            (email.length < 5 || email.length > 100)  -> return " should be between 5 and 100 characters!"
            (!email.contains("@"))  -> return " should contain @ character!"
            (!Regex(pattern = """.{1,}@[\w\d.-_]+[.].+[\w\d.-_]""").containsMatchIn(input = email)) -> return " is incorrect!"
            else -> return "OK"
        }
    }
    private fun validatePhone(phone: String): String{
        when {
            (phone.length == 0 || phone == " ") -> return " could't be blank!"
            (phone.length < 8 || phone.length > 32)  -> return " should be between 8 and 32 characters!"
            (Regex(pattern = """[^\d+]+""").containsMatchIn(input = phone)) -> return " couldn't consist of non-numbers!"
            else -> return "OK"
        }
    }

}

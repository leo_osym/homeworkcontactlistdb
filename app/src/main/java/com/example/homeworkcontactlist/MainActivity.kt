package com.example.homeworkcontactlist

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import android.support.v7.app.AlertDialog
import android.view.MenuItem
import android.view.View


class MainActivity : AppCompatActivity() {

    var contacts = ArrayList<Contact>()
    lateinit var adapter: ContactsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        contacts = DBHelper.getAllContacts()

        setRecyclerViewAdapter()

        // floating action button - for adding new contact
        val fab: View = findViewById(R.id.fab)
        fab.setOnClickListener { _ ->
            val intent = Intent(baseContext, AddEditActivity::class.java)
            intent.putExtra("ID", -1)
            intent.putExtra("EDIT", false)
            startActivity(intent)
        }
    }

    private fun setRecyclerViewAdapter(){
        adapter = ContactsAdapter(contacts, this)
        // add separator line
        recyclerView.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerView.layoutManager = LinearLayoutManager(this)

        adapter.setListener(object : ContactsAdapter.Listener {
            override fun onClick(position: Int) {
                val intent = Intent(baseContext, DetailsActivity::class.java)
                intent.putExtra("ID", contacts[position].Id)
                startActivity(intent)
            }
        })
        recyclerView.adapter = adapter
        registerForContextMenu(recyclerView)
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        when (item!!.groupId) {
            0 -> { // Details
                goToDetails(item)
            }
            1 -> { // Edit
                goToEdit(item)
            }
            2 -> { // Delete
                deleteItem(item)
            }
        }
        return true
    }

    // go to DetailsActivity
    private fun  goToDetails(item: MenuItem?){
        var id = 0
         if (item != null) {
            id = contacts[item.itemId].Id
        }
        val intent = Intent(baseContext, DetailsActivity::class.java)
        intent.putExtra("ID", id)
        startActivity(intent)
    }

    // go to AddEditActivity
    private fun  goToEdit(item: MenuItem?){
        if(item != null) {
            val intent = Intent(baseContext, AddEditActivity::class.java)
            intent.putExtra("ID", contacts[item.itemId].Id)
            intent.putExtra("EDIT", true)
            startActivity(intent)
        }
    }

    // delete contact from db
    private fun deleteItem(item: MenuItem?){
        if(item != null) {
            var builder = AlertDialog.Builder(this)
            builder.setTitle("Delete contact")
                .setMessage("Do you really want to delete this contact?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .setNegativeButton("Cancel") { _, _ -> }
                .setPositiveButton("Yes") { _, _ ->
                    var res = DBHelper.deleteContact(contacts[item.itemId].Id)
                    contacts.removeAt(item.itemId)
                    adapter.notifyDataSetChanged()
                    // if operation successfull
                    if(res) {
                        var notification = NotificationHelper()
                        var notificationIntent = Intent(this, LoginActivity::class.java)
                        notification.makeNotifications(this, R.drawable.delete_white_24dp,"Contact deleted","Contact was successfully deleted!", notificationIntent)
                    }
                }
            var alert = builder.create()
            alert.show()
        }
    }

}
